import React from 'react'
import { createStore } from './createStore'
import { useLocalStore } from 'mobx-react'

const storeContext = React.createContext({ hydrated: false })

export const StoreProvider = ({ children }) => {
  const store = useLocalStore(createStore)
  return <storeContext.Provider value={store}>{children}</storeContext.Provider>
}

export const useStore = () => {
  const store = React.useContext(storeContext)
  if (!store) {
    throw new Error('useStore must be used within a StoreProvider.')
  }
  return store
}

export const usePlacesStore = () => {
  const store = useStore()
  return store.places
}

export const useLocksStore = () => {
  const store = useStore()
  return store.locks
}

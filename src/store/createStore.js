import { types } from 'mobx-state-tree'
import Kisi from 'kisi-client'

const kisiClient = new Kisi()
kisiClient.setLoginSecret('94c2056abb993b570517f2d3a89c9b5a')

const Pagination = types.model({
  count: types.number,
  limit: types.number,
  offset: types.number,
})

const Lock = types.model({
  id: types.number,
  name: types.maybeNull(types.string),
  open: false,
  unlock_duration: types.number,
})

const Locks = types
  .model({
    locks: types.array(Lock),
    pagination: types.maybe(Pagination),
    loading: false,
    unlocking: false,
    unlockDuration: 0,
    success: false,
    error: '',
  })
  .actions(self => ({
    fetchLocks(placeId) {
      self.loading = true
      return kisiClient
        .get('locks', { place_id: placeId, online: true })
        .then(self.setLocks)
        .catch(self.handleError)
    },
    setLocks(result) {
      self.loading = false
      return (self.locks = result.data.map(l => {
        const { id, name, open, unlock_duration } = l
        return { id, name, open, unlock_duration }
      }))
    },
    unLock(id) {
      self.unlocking = true
      return kisiClient
        .post(`locks/${id}/unlock`)
        .then(self.unlockSuccess)
        .catch(self.handleError)
    },
    unlockSuccess(data) {
      self.success = true
      self.unlockDuration = data.unlock_duration * 1000
    },
    handleError(e) {
      self.unlocking = false
      self.loading = false
      self.error = e.reason
    },
    clearError() {
      self.error = ''
      self.success = false
      self.unlockDuration = 0
    },
  }))

const Place = types.model({
  id: types.number,
  name: types.maybeNull(types.string),
  description: types.maybeNull(types.string),
})

const Places = types
  .model({
    places: types.array(Place),
    pagination: types.maybe(Pagination),
    loading: false,
    error: '',
  })
  .actions(self => ({
    fetchPlaces() {
      self.loading = true
      return (
        kisiClient
          .get('user')
          .then(user => {
            return kisiClient.get('places', { user_id: user.id })
          })
          // .get('places')
          .then(self.setPlaces)
          .catch(self.handleError)
      )
    },
    setPlaces(result) {
      self.loading = false
      return (self.places = result.data.map(r => {
        const { id, name, description } = r
        return { id, name, description }
      }))
    },
    handleError(e) {
      self.loading = false
      self.error = e.reason
    },
    clearError() {
      self.error = ''
    },
  }))

const Store = types.model({
  places: types.optional(Places, {
    places: [],
    pagination: { count: 0, limit: 0, offset: 0 },
  }),
  locks: types.optional(Locks, {
    locks: [],
    pagination: { count: 0, limit: 0, offset: 0 },
  }),
})

export function createStore() {
  return Store.create()
}

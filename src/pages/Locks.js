import React, { useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import {
  Container,
  AppBar,
  Toolbar,
  Typography,
  Card,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  CircularProgress,
  ListItemSecondaryAction,
  Switch,
  Snackbar,
  SnackbarContent,
  IconButton,
} from '@material-ui/core'
import { ArrowBack } from '@material-ui/icons'
import { green } from '@material-ui/core/colors'
import { useLocksStore } from '../store/store'
import { useParams } from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: 100,
  },
  spinner: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  success: {
    backgroundColor: green[600],
  },
}))

const LockListItem = ({ id, name, open, onChange }) => (
  <ListItem divider>
    <ListItemText primary={name} secondary={open ? 'Open' : 'Locked'}></ListItemText>
    <ListItemSecondaryAction>
      <Switch edge="end" checked={!open} onChange={() => onChange(id)}></Switch>
    </ListItemSecondaryAction>
  </ListItem>
)

export default observer(() => {
  const classes = useStyles()
  const store = useLocksStore()
  const { id } = useParams()
  useEffect(() => {
    store.fetchLocks(id)
    return () => {
      store.clearError()
    }
  }, [])
  const items = store.locks.map((p, i) => <LockListItem key={i} {...p} onChange={store.unLock} />)
  return (
    <React.Fragment>
      <Container>
        <AppBar position="absolute">
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={() => window.history.go(-1)}>
              <ArrowBack />
            </IconButton>
            <Typography>Locks</Typography>
          </Toolbar>
        </AppBar>
        <Card className={classes.card}>
          {store.loading ? (
            <div className={classes.spinner}>
              <CircularProgress />
            </div>
          ) : (
            <List>
              {items.length > 0 ? (
                items
              ) : (
                <ListItem>
                  <ListItemText>No locks online</ListItemText>
                </ListItem>
              )}
            </List>
          )}
        </Card>
      </Container>
      <Snackbar open={store.unlocking}>
        <SnackbarContent message="Unlocking..."></SnackbarContent>
      </Snackbar>
      <Snackbar open={store.error !== ''} autoHideDuration={3000} onClose={store.clearError}>
        <SnackbarContent className={classes.error} message={store.error}></SnackbarContent>
      </Snackbar>
      <Snackbar open={store.success} autoHideDuration={store.unlockDuration} onClose={store.clearError}>
        <SnackbarContent className={classes.success} message={'Lock unlocked!'}></SnackbarContent>
      </Snackbar>
    </React.Fragment>
  )
})

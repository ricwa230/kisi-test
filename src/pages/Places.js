import React, { useEffect } from 'react'
import { observer } from 'mobx-react-lite'
import {
  Container,
  AppBar,
  Toolbar,
  Typography,
  Card,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  CircularProgress,
  ListItemSecondaryAction,
  IconButton,
  Snackbar,
  SnackbarContent,
} from '@material-ui/core'
import { usePlacesStore } from '../store/store'
import { ChevronRight } from '@material-ui/icons'

const useStyles = makeStyles(theme => ({
  card: {
    marginTop: 100,
  },
  spinner: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
}))

const PlaceListItem = ({ id, name }) => (
  <ListItem button divider onClick={() => (window.location.href = `#/place/${id}`)}>
    <ListItemText primary={name}></ListItemText>
    <ListItemSecondaryAction>
      <IconButton edge="end">
        <ChevronRight></ChevronRight>
      </IconButton>
    </ListItemSecondaryAction>
  </ListItem>
)

export default observer(() => {
  const classes = useStyles()
  const store = usePlacesStore()
  useEffect(() => {
    store.fetchPlaces()
  }, [])
  const items = store.places.map((p, i) => <PlaceListItem key={i} {...p} />)
  return (
    <React.Fragment>
      <Container>
        <AppBar position="absolute">
          <Toolbar>
            <Typography>Places</Typography>
          </Toolbar>
        </AppBar>
        <Card className={classes.card}>
          {store.loading ? (
            <div className={classes.spinner}>
              <CircularProgress />
            </div>
          ) : (
            <List>
              {items.length > 0 ? (
                items
              ) : (
                <ListItem>
                  <ListItemText>No places found</ListItemText>
                </ListItem>
              )}
            </List>
          )}
        </Card>
      </Container>
      <Snackbar open={store.error !== ''} autoHideDuration={3000} onClose={store.clearError}>
        <SnackbarContent className={classes.error} message={store.error}></SnackbarContent>
      </Snackbar>
    </React.Fragment>
  )
})

import React from 'react'
import { HashRouter as Router, Route } from 'react-router-dom'
import Places from './pages/Places'
import Locks from '././pages/Locks'

const App = () => {
  return (
    <Router>
      <Route exact path="/">
        <Places />
      </Route>
      <Route exact path="/place/:id">
        <Locks />
      </Route>
    </Router>
  )
}

export default App
